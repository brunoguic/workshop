from influxdb import InfluxDBClient
import pika
import random
# pings_monitoring,terminal_model=S920 count=5,hour=15,week_day=02,month=05,year=2018


#"time": "2018-05-25T21:00:00Z",

def rabbitMQ_connection(host, port, virtual_host, user, password):
	credentials = pika.PlainCredentials(user, password)
	return pika.BlockingConnection(
		pika.ConnectionParameters(host=host, port=port, virtual_host=virtual_host, credentials=credentials))



def rabbitMQ_callback(ch, method, properties, body):
    print(" [x] Received %r" % body)
    query = influx_json_query(body)
    Influx_client.write_points(query)

def influx_insert(body):
    #print(" [x] Received %r" % body)
    query = influx_json_query(body)
    Influx_client.write_points(query)    




def influx_json_query(data):
	json_body = [
        {
			"measurement": "some_event",
			"tags": {
				"generator": "GAUSSIAN"
			},

			"fields": {
				"thick" : float(data),
				"week_day": random.choice(list(range(1,7))),
				"hour": random.choice(list(range(0,23))),
				"day": random.choice(list(range(1,30))),
				"month": random.choice(list(range(1,12))),
				"year": 2018
			}
		}
	]

	return json_body

Influx_client = InfluxDBClient('localhost', 8086, 'workshop', 'workshop', 'timeseries')

rabbitMQ_host = 'localhost'
rabbitMQ_port = 5672
rabbitMQ_virtualhost = '/'
rabbitMQ_user = 'admin'
rabbitMQ_password = 'password'
rabbitMQ_queue = 'outliers'
rabbitMQ_isDurable = True


rabbitMQ_connection = rabbitMQ_connection(
	rabbitMQ_host,
	rabbitMQ_port,
	rabbitMQ_virtualhost,
	rabbitMQ_user,
	rabbitMQ_password
)


rabbitMQ_channel = rabbitMQ_connection.channel()
rabbitMQ_channel.basic_qos(prefetch_count=1)
rabbitMQ_channel.queue_declare(queue=rabbitMQ_queue, durable=rabbitMQ_isDurable)


#rabbitMQ_channel.basic_consume(rabbitMQ_callback,
#                      queue=rabbitMQ_queue,
#                      no_ack=True)

print(' [*] Waiting for messages. To exit press CTRL+C')
#rabbitMQ_channel.start_consuming()

for method_frame, properties, body in rabbitMQ_channel.consume(rabbitMQ_queue):

	influx_insert(body)

	# Acknowledge the message
	rabbitMQ_channel.basic_ack(method_frame.delivery_tag)

# client.create_database('telecom')


    

    







#result = client.query("SELECT pings FROM pings_monitoring WHERE week_day = 1 ORDER BY time DESC LIMIT 8;")


#print("Result from library: {0}".format(result[0]))


#def checkInflux():
#    url = "http://10.152.20.212:8086/query?q=select+*+from+pings_monitoring&db=telecom"
#    r = requests.get(url)
#    print(r.status_code)
#    print(r.encoding)
#    print(r.json())


#def getLastReport():
#    query = "select  * from pings_monitoring order by time desc limit 1"



#current_counter = 1200
#def checkAlarm (lista_limpa, current_counter, treshold):
#    std_dev = standard_deviation(lista_limpa)
#    lower_bound  = treshold * (average - standard_deviation)
#    upper_bound = treshold * (average + standard_deviation)
#    return True

#def standard_deviation(lista_limpa):
#    return std_dev;

#for each terminal_model:
#    perfil_sujo = [1240, 1435, 53243, 34253, 342345, 432354, 234354, 12343]
#    outliers_indexes = getOutliers(perfil_sujo)
#    lista_limpa = median_filter(outliers_indexes, perfil_sujo)
#    is_alarm =  checkAlarm (lista_limpa, current_counter)
#
#    if is_alarm == True:
 #       print("olha o alarme ai!")

